#!/bin/bash

set -e

NAMESPACE=axelsommerfeldt

#podman build --pull --no-cache --build-arg BASE_IMAGE=ubuntu:18.04 -t ${NAMESPACE}/texlive:ubuntu_18.04 .
#podman tag ${NAMESPACE}/texlive:ubuntu_18.04 ${NAMESPACE}/texlive:ubuntu_bionic

#podman build --pull --no-cache --build-arg BASE_IMAGE=ubuntu:20.04 -t ${NAMESPACE}/texlive:ubuntu_20.04 .
#podman tag ${NAMESPACE}/texlive:ubuntu_20.04 ${NAMESPACE}/texlive:ubuntu_focal

#podman build --pull --no-cache --build-arg BASE_IMAGE=ubuntu:22.04 -t ${NAMESPACE}/texlive:ubuntu_22.04 .
#podman tag ${NAMESPACE}/texlive:ubuntu_22.04 ${NAMESPACE}/texlive:ubuntu_jammy

podman build --pull --no-cache --build-arg BASE_IMAGE=ubuntu:24.04 -t ${NAMESPACE}/texlive:ubuntu_24.04 .
podman tag ${NAMESPACE}/texlive:ubuntu_24.04 ${NAMESPACE}/texlive:ubuntu_noble 

