#!/bin/bash

set -e

NAMESPACE=axelsommerfeldt

podman login --username ${NAMESPACE} docker.io

#podman push ${NAMESPACE}/texlive:ubuntu_18.04
#podman push ${NAMESPACE}/texlive:ubuntu_bionic

#podman push ${NAMESPACE}/texlive:ubuntu_20.04
#podman push ${NAMESPACE}/texlive:ubuntu_focal

#podman push ${NAMESPACE}/texlive:ubuntu_22.04
#podman push ${NAMESPACE}/texlive:ubuntu_jammy

podman push ${NAMESPACE}/texlive:ubuntu_24.04
podman push ${NAMESPACE}/texlive:ubuntu_noble

podman logout

