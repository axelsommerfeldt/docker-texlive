# docker-texlive

Ubuntu-based TeXlive docker images used in GitLab CI for my LaTeX projects:

* https://gitlab.com/axelsommerfeldt/caption
* https://gitlab.com/axelsommerfeldt/endfloat
* https://gitlab.com/axelsommerfeldt/newfloat
* https://gitlab.com/axelsommerfeldt/totalcount
