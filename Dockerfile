ARG BASE_IMAGE=ubuntu:22.04
ARG DEBIAN_FRONTEND=noninteractive

FROM ${BASE_IMAGE}
MAINTAINER Axel Sommerfeldt <axel.sommerfeldt@f-m.fm>

RUN ln -snf /usr/share/zoneinfo/Etc/UTC /etc/localtime \
 && echo "Etc/UTC" > /etc/timezone \
 && apt-get -qy update \
 && apt-get -qy install biber ghostscript texlive-full \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

VOLUME ["/work"]
WORKDIR /work

CMD ["bash"]

# See also: https://tex.stackexchange.com/questions/391151/does-an-official-recommended-docker-image-exist-for-texlive-full-pdflatex
